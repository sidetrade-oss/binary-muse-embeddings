# Binary MUSE embeddings

Hosting for aligned word embeddings from [Facebook's MUSE](https://github.com/facebookresearch/MUSE), saved in a binary format for faster retrieval and loading.

We use [msgpack](https://msgpack.org/), a fast and compact serialization format, to efficiently serialize the models in a binary format.

The models are **5x smaller** and load **100x faster** (typically in 150ms instead of 15s).

## Languages available

*NB: at the moment, the Arabic model is not available, due to an [issue](https://github.com/facebookresearch/MUSE/issues/150) on the MUSE original model.*

| | | | | | |
|---|---|---|---|---|---|
| Arabic: NOT AVAILABLE | Bulgarian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_bg.bin?inline=false) | Catalan: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_ca.bin?inline=false) | Croatian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_hr.bin?inline=false) | Czech: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_cs.bin?inline=false) | Danish: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_da.bin?inline=false)
| Dutch: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_nl.bin?inline=false) | English: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_en.bin?inline=false) | Estonian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_et.bin?inline=false) | Finnish: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_fi.bin?inline=false) | French: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_fr.bin?inline=false) | German: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_de.bin?inline=false)
| Greek: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_el.bin?inline=false) | Hebrew: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_he.bin?inline=false) | Hungarian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_hu.bin?inline=false) | Indonesian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_id.bin?inline=false) | Italian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_it.bin?inline=false) | Macedonian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_mk.bin?inline=false)
| Norwegian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_no.bin?inline=false) | Polish: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_pl.bin?inline=false) | Portuguese: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_pt.bin?inline=false) | Romanian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_ro.bin?inline=false) | Russian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_ru.bin?inline=false) | Slovak: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_sk.bin?inline=false)
| Slovenian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_sl.bin?inline=false) | Spanish: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_es.bin?inline=false) | Swedish: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_sv.bin?inline=false) | Turkish: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_tr.bin?inline=false) | Ukrainian: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_uk.bin?inline=false) | Vietnamese: [*bin*](https://gitlab.com/sidetrade-oss/binary-muse-embeddings/raw/master/models/model_vi.bin?inline=false)



## How to use

After downloading the models, you can use the `load_vec_bin` function below to load your model.
The output of the function is the same as the output of the `load_vec` function defined in the [demo notebook of the MUSE repository](https://github.com/facebookresearch/MUSE/blob/master/demo.ipynb).

``` python
import numpy as np
import msgpack

def load_vec_bin(filepath):
    with open(filepath, "rb") as f:
        shape, buffer, word2id = msgpack.load(f, encoding="UTF-8")
    embedding = np.ndarray(shape, np.float16, buffer)
    return embedding, word2id

emb_bin, dict_bin = load_vec_bin("./model_en.bin")
```

The gain in size is mostly due to representing the coefficient with a `float16` dtype instead of the default `float64`. This causes a small representation error (around 10^-5 on average), which is acceptable.

You can look at the [FastModelLoading notebook](./FastModelNotebook.ipynb) for additional details, and to build your own binary, more detailed representation using `float32` if you wish. Bear in mind that it would result in twice-as-large models (around 250MB).

![alt text](error_distribution.png "Error distribution")

## Dependencies

See in the `requirements.txt` file.

* Python 3
* msgpack and numpy

